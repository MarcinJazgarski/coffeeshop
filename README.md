programowanie obiektowe

Stworzymy model prostego systemu, jakim posługują się np. popularne fastfoody

Klasy produktów powinny mieć hierarchię, klasami abstrakcyjnymi nie są tylko klasy konkretnych produktów.

Pakiet com.sda.model wypełnia lista typowych POJO (posiadają jedynie pola, konstruktory i gettery). Wszystkie powinny posiadać gettery dla swoich pól.

• Klasa abstrakcyjna Product –nadklasa wszystkich możliwych produktów, które można zamówić w kawiarni. Powinna posiadać pole:
private BigDecimal price; i konstruktor wypełniający te pole.

• Enum Extra – enum wyliczający możliwe dodatki do zamówienia (SUGAR, MILK, ESPRESSO, COCOA, CREAM, WAFER) z dowolnie dobranymi cenami ustawianymi przez pole private BigDecimal price; i konstruktor wypełniający te pole.

• Klasa finalna Order –klasa zamówienia, powinna posiadać pola: private final long oredrID; private final List<Product> products; private final List<Extra> extras; private final BigDecimal price; i jedyny konstruktor – wypełniający wszystkie pola
W podpakiecie com.sda.model.drink:
o Klasa abstrakcyjna Drink – nadklasa napojów, dziedziczy z Product. Powinna zawierać pole private boolean syrup; i konstruktor wypełniający te pole i pola nadklasy.
o Klasa abstrakcyjna Coffee – nadklasa kaw, dziedziczy z Drink. Powinna zawierać pole: private boolean soyMilk; i konstruktor wypełniający te pole i pola nadklasy.
o Klasa abstrakcyjna Tea – nadklasa herbat, dziedziczy z Drink. Powinna zawierać pole: private int lemon; i konstruktor wypełniający te pole i pola nadklasy.
o Klasa IceLatte – klasa produktu, dziedziczy z Coffee. Powinna zawierać pole: private int iceCubes; i konstruktor wypełniający te pole i pola nadklasy, a price ustawiać na sztywno.
o Klasa Mocha – klasa produktu, dziedziczy z Coffee. Powinna zawierać pole: private boolean seasoning; i konstruktor wypełniający te pole i pola nadklasy, a price ustawiać na sztywno.
o Klasa IceTea – klasa produktu, dziedziczy z Tea. Powinna zawierać pole: private boolean mint; i konstruktor wypełniający te pole i pola nadklasy, a price ustawiać na sztywno.
o Klasa MintTea – klasa produktu, dziedziczy z Tea. Powinna zawierać pole: private boolean longBrewed; i konstruktor wypełniający te pole i pola nadklasy, a price ustawiać na sztywno.

• W podpakiecie com.sda.model.food:
o Klasa abstrakcyjna Food – nadklasa jedzenia, dziedziczy z Product. Powinna zawierać pole: private boolean cream; i konstruktor wypełniający te pole i pola nadklasy.
o Klasa abstrakcyjna Cake – nadklasa ciast, dziedziczy z Food. Powinna zawierać pole: private boolean chocolateIcing; i konstruktor wypełniający te pole i pola nadklasy.
o Klasa abstrakcyjna IceCream – nadklasa lodów, dziedziczy z Food. Powinna zawierać pole:
private int wafers; i konstruktor wypełniający te pole i pola nadklasy.
o Klasa Brownie – klasa produktu, dziedziczy z Cake. Powinna zawierać pole: private boolean chutney; i konstruktor wypełniający te pole i pola nadklasy, a price ustawiać na sztywno.
o Klasa Cheesecake – klasa produktu, dziedziczy z Cake. Powinna zawierać pole: private boolean freshFruits; i konstruktor wypełniający te pole i pola nadklasy, a price ustawiać na sztywno.
o Klasy IceCreamChocolate i IceCreamVanilla – klasy produktu, dziedziczą z IceCream. Powinny zawierać konstruktor wypełniający pola nadklasy, a price ustawiać na sztywno.

Pakiet com.sda.service zawiera klasę OrderService, której zadaniem powinno być wyliczenie ceny, numeru zamówienia i stworzenie obiektu zamówienia. Klasa OrderService powinna zawierać:
• pole private long currentOrderNumber;
• bezparametrowy konstruktor ustawiający powyższe pole na 0,
• metodę public Order createOrder(List<Product products, List<Extra> extras), która:
▪ sprawdzi czy przekazywana lista produktów jest nullem i jest pusta i w takim wypadku rzuci odpowiedni wyjątek,
▪ obliczy sumę cen produktów i dodatków z podanych list,
▪ stworzy nowy obiekt zamówienia (na podstawie list, wyliczonej ceny i jako orderID podając currentOrderNumber),
▪ zinkrementuje currentOrderNumber,
▪ zwróci obiekt zamówienia.

Pakiet com.sda zawiera klasę CoffeeShop, klasę kawiarni (główną), która powinna zarządzać zamówieniami i tworzyć je przy użyciu OrderService. Klasa powinna zawierać:
• pole private OrderService orderService;
• pole private Map<Long, Order> ordersPending; (lista zamówień oczekujących),
• pole private Map<Long, Order> ordersDone; (lista zamówień wykonanych),
• konstruktor bezparametrowy, który zainicjalizuje listy i OrderService,
• metodę public Order createOrder(List<Product> products, List<Extra> extras), która wywoła metodę tworzenia zamówienia z OrderService i doda utworzone zamówienie do listy oczekujących,
• metodę public void orderDone(long orderID), która przeniesie zamówienie z listy oczekujących na listę wykonanych).