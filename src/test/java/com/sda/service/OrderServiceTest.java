package com.sda.service;

import com.sda.model.Extra;
import com.sda.model.Product;
import com.sda.model.food.IceCreamChocolate;
import com.sda.model.food.IceCreamVanilla;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class OrderServiceTest {

    @Test
    public void shouldPass() {
        // Given
        IceCreamVanilla iceCreamVanilla = new IceCreamVanilla(true,10);
        IceCreamChocolate iceCreamChocolate = new IceCreamChocolate(true,20);

        Extra extra1 = Extra.SUGAR;
        Extra extra2 = Extra.COCOA;

        ArrayList<Product> products = new ArrayList<>();
        products.add(iceCreamVanilla);
        products.add(iceCreamChocolate);

        ArrayList<Extra> extras = new ArrayList<>();
        extras.add(extra1);
        extras.add(extra2);

        OrderService orderService = new OrderService();

        // When
        long orderID = orderService.createOrder(products, extras).getOrderID();
        BigDecimal price =  orderService.createOrder(products, extras).getPrice();

        // Then
        assertEquals(orderID, 0);
        assertEquals(price, new BigDecimal("20.80"));

    }

}