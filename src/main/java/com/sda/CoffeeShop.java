package com.sda;

import com.sda.model.Extra;
import com.sda.model.Order;
import com.sda.model.Product;
import com.sda.service.OrderService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoffeeShop {

    private OrderService orderService;
    private Map<Long, Order> ordersPending;
    private Map<Long, Order> ordersDone;

    public CoffeeShop() {
        this.orderService = new OrderService();
        this.ordersPending = new HashMap<>();
        this.ordersDone = new HashMap<>();
    }

    public Order createOrder(List<Product> products, List<Extra> extras) {
        Order order = this.orderService.createOrder(products, extras);
        this.ordersPending.put(order.getOrderID(),order);
        return order;
    }

    public void orderDone(long orderID){
        this.ordersDone.put(orderID,ordersPending.get(orderID));
        this.ordersPending.remove(orderID);
    }

    public Map<Long, Order> getOrdersPending() {
        return ordersPending;
    }

    public Map<Long, Order> getOrdersDone() {
        return ordersDone;
    }




}
