package com.sda.service;

import com.sda.model.EmptyOrderException;
import com.sda.model.Extra;
import com.sda.model.Order;
import com.sda.model.Product;

import java.math.BigDecimal;
import java.util.List;

public class OrderService {

    private long currentOrderNumber;

    public long getCurrentOrderNumber() {
        return currentOrderNumber;
    }

    public OrderService() {
        this.currentOrderNumber = 0;
    }

    public Order createOrder(List<Product> products, List<Extra> extras) throws EmptyOrderException {

        if (products == null || products.isEmpty()) {
            throw new EmptyOrderException();
        }

        BigDecimal productPrice = new BigDecimal("0.00");
        for (Product product : products) {
            productPrice = productPrice.add(product.getPrice());
        }

        BigDecimal extrasPrice = new BigDecimal("0.00");
        for (Extra extra : extras) {
            extrasPrice = extrasPrice.add(extra.getPrice());
        }

        Order order = new Order(getCurrentOrderNumber(), products, extras, productPrice.add(extrasPrice));
        currentOrderNumber++;
        return order;
    }


}
