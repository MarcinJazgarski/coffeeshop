package com.sda;

import com.sda.model.Extra;
import com.sda.model.Order;
import com.sda.model.Product;
import com.sda.model.drink.Mocha;
import com.sda.model.food.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        CoffeeShop coffeeShop = new CoffeeShop();

        List<Product> products = new ArrayList<>();
        List<Extra> extras = new ArrayList<>();

        products.add(new IceCreamChocolate(true, 50));
        products.add(new Brownie(false, true, false));
        products.add(new CheeseCake(true, true, true));

        extras.add(Extra.COCOA);
        extras.add(Extra.CREAM);

        Order order = coffeeShop.createOrder(products, extras);

        Order order1 = coffeeShop.createOrder(
                Arrays.asList(new Mocha(true, true, true), new Brownie(false, false, false)),
                Arrays.asList(Extra.COCOA, Extra.SUGAR));

        Order order2 = coffeeShop.createOrder(
                Arrays.asList(new Brownie(true,true,true)),
                Arrays.asList(Extra.CREAM));

        System.out.println(order.getOrderID());
        System.out.println(order.getPrice());
        System.out.println(order1.getOrderID());
        System.out.println(order1.getPrice());
        System.out.println(order2.getOrderID());
        System.out.println(order2.getPrice());

    }


}
