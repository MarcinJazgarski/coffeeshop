package com.sda.model;

public class EmptyOrderException extends RuntimeException {

    public EmptyOrderException() {
        super("Empty Order!");
    }
}
