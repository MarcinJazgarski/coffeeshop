package com.sda.model;

import java.math.BigDecimal;

public enum Extra {

    SUGAR(BigDecimal.ONE),
    MILK(BigDecimal.ZERO),
    ESPRESSO(BigDecimal.ZERO),
    COCOA(BigDecimal.TEN),
    CREAM(BigDecimal.ZERO),
    WATER(BigDecimal.ZERO);

    private BigDecimal price;

    Extra(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
