package com.sda.model.drink;

import java.math.BigDecimal;

public class MintTea extends Tea{

    private boolean longBrewed;

    public MintTea(boolean syrup, int lemon, boolean longBrewed) {
        super(new BigDecimal("7.50"), syrup, lemon);
        this.longBrewed = longBrewed;
    }

    public boolean isLongBrewed() {
        return longBrewed;
    }
}
