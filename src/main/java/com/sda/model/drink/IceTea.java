package com.sda.model.drink;

import java.math.BigDecimal;

public class IceTea extends Tea{

    private boolean mint;

    public IceTea(boolean syrup, int lemon, boolean mint) {
        super(new BigDecimal("6.90"), syrup, lemon);
        this.mint = mint;
    }

    public IceTea(boolean mint) {
        this.mint = mint;
    }
}
