package com.sda.model.drink;

import java.math.BigDecimal;

public class Mocha extends Coffee{

    private boolean seasoning;

    public Mocha(boolean syrup, boolean soyMilk, boolean seasoning) {
        super(new BigDecimal("9.90"), syrup, soyMilk);
        this.seasoning = seasoning;
    }

    public boolean isSeasoning() {
        return seasoning;
    }
}
