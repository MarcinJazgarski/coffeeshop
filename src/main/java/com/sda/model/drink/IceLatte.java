package com.sda.model.drink;

import java.math.BigDecimal;

public class IceLatte extends Coffee {

    private int iceCubes;

    public IceLatte(boolean syrup, boolean soyMilk, int iceCubes) {
        super(new BigDecimal("10.90"), syrup, soyMilk);
        this.iceCubes = iceCubes;
    }

    public int getIceCubes() {
        return iceCubes;
    }

}
