package com.sda.model.food;

import java.math.BigDecimal;

public class Brownie extends Cake{

    private boolean chutney;

    public Brownie(boolean cream, boolean chocolateIcing, boolean chutney) {
        super(new BigDecimal("9.90"), cream, chocolateIcing);
        this.chutney = chutney;
    }

    public boolean isChutney() {
        return chutney;
    }

}
