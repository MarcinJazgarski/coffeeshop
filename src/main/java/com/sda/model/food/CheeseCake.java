package com.sda.model.food;

import java.math.BigDecimal;

public class CheeseCake extends Cake{

    private boolean freshFruits;

    public CheeseCake(boolean cream, boolean chocolateIcing, boolean freshFruits) {
        super(new BigDecimal("12.90"), cream, chocolateIcing);
        this.freshFruits = freshFruits;
    }

    public boolean isFreshFruits() {
        return freshFruits;
    }
}
