package com.sda.model.food;

import java.math.BigDecimal;

public abstract class IceCream extends Food {

    private int wafers;

    public IceCream() {
    }

    public IceCream(BigDecimal price, boolean cream, int wafers) {
        super(price, cream);
        this.wafers = wafers;
    }

    public int getWafers() {
        return wafers;
    }
}
