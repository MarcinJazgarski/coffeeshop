package com.sda.model.food;

import java.math.BigDecimal;

public abstract class Cake extends Food{

    private boolean chocolateIcing;

    public Cake() {
    }

    public Cake(BigDecimal price, boolean cream, boolean chocolateIcing) {
        super(price, cream);
        this.chocolateIcing = chocolateIcing;
    }

    public boolean isChocolateIcing() {
        return chocolateIcing;
    }
}
