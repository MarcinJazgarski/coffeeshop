package com.sda.model.food;

import java.math.BigDecimal;

public class IceCreamChocolate extends IceCream{

    public IceCreamChocolate(boolean cream, int wafers) {
        super(new BigDecimal("4.90"), cream, wafers);
    }
}
