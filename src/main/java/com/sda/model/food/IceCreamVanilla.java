package com.sda.model.food;

import java.math.BigDecimal;

public class IceCreamVanilla extends IceCream{

    public IceCreamVanilla(boolean cream, int wafers) {
        super(new BigDecimal("4.90"), cream, wafers);
    }
}
